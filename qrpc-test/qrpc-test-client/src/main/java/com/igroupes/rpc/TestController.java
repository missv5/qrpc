package com.igroupes.rpc;

import com.igroupes.rpc.config.annotation.Reference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("test")
public class TestController {

    @Reference
    private UserService userService;

    @GetMapping("/user")
    public User getUser(@RequestParam("id")Long id){
        return userService.getUser(id);
    }
}
