package com.igroupes.rpc;

public interface UserService {
    User getUser(Long id);
}
