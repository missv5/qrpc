package com.igroupes.rpc.exception;

public class QrpcException extends RuntimeException {

    public QrpcException(String msg) {
        super(msg);
    }
}
