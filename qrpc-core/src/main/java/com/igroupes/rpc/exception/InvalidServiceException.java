package com.igroupes.rpc.exception;

public class InvalidServiceException extends QrpcException{
    public InvalidServiceException(String msg) {
        super(msg);
    }
}
