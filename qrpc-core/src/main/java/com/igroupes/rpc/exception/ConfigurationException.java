package com.igroupes.rpc.exception;

public class ConfigurationException extends QrpcException{
    public ConfigurationException(String msg) {
        super(msg);
    }
}
