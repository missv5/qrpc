package com.igroupes.rpc.exception;

public class SerializeException extends QrpcException{
    public SerializeException(String msg) {
        super(msg);
    }
}
