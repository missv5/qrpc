package com.igroupes.rpc.exception;

public class ServiceNameExistsException extends QrpcException{
    public ServiceNameExistsException(String msg) {
        super(msg);
    }
}
