package com.igroupes.rpc.exception;

import com.igroupes.rpc.common.entity.ResponseMessage;

public class RpcServerException extends QrpcException {
    private ResponseMessage.Code code;

    public RpcServerException(String msg, ResponseMessage.Code code) {
        super(msg);
        this.code = code;
    }

    public ResponseMessage.Code getCode() {
        return code;
    }
}
