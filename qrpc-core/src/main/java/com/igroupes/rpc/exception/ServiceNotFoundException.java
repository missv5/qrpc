package com.igroupes.rpc.exception;

public class ServiceNotFoundException extends QrpcException{
    public ServiceNotFoundException(String msg) {
        super(msg);
    }
}
