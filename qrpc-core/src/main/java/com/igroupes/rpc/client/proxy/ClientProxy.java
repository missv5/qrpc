package com.igroupes.rpc.client.proxy;

public interface ClientProxy {

    public Object getRpcProxy(String serviceName,Class<?> serviceClazz) ;
}
