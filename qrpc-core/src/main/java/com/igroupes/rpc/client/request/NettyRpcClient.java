package com.igroupes.rpc.client.request;

import com.igroupes.rpc.common.entity.RequestMessage;
import com.igroupes.rpc.common.entity.ResponseMessage;
import com.igroupes.rpc.common.entity.ServiceEntity;
import com.igroupes.rpc.common.message.MessageSerializer;
import com.igroupes.rpc.config.RpcProperties;
import com.igroupes.rpc.utils.FutureUtils;
import com.igroupes.rpc.utils.Requires;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.stream.ChunkedWriteHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.nio.channels.ClosedChannelException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

/**
 * 参考： flink源码 org.apache.flink.queryablestate.network.Client
 */
@Slf4j
public class NettyRpcClient implements RpcClient {
    private final Bootstrap bootstrap;
    private final RpcProperties rpcProperties;
    /**
     * Established connections.
     */
    private final Map<String, EstablishedConnection> establishedConnections = new ConcurrentHashMap<>();

    /**
     * Pending connections.
     */
    private final Map<String, PendingConnection> pendingConnections = new ConcurrentHashMap<>();

    /**
     * Atomic shut down future.
     */
    private final AtomicReference<CompletableFuture<Void>> clientShutdownFuture = new AtomicReference<>(null);

    public NettyRpcClient(RpcProperties rpcProperties) {
        int numEventLoopThreads = rpcProperties.getClientNumEventLoopThreads();
        Requires.requireTrue(numEventLoopThreads >= 1,
                "Non-positive number of event loop threads.");

        final ThreadFactory threadFactory = new ThreadFactoryBuilder()
                .setDaemon(true)
                .build();
        this.rpcProperties = rpcProperties;

        final EventLoopGroup nioGroup = new NioEventLoopGroup(numEventLoopThreads, threadFactory);

        this.bootstrap = new Bootstrap()
                .group(nioGroup)
                .channel(NioSocketChannel.class)
                .handler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel channel) throws Exception {
                        channel.pipeline()
                                .addLast(new LengthFieldBasedFrameDecoder(Integer.MAX_VALUE, 0, 4, 0, 4))
                                .addLast(new ChunkedWriteHandler());
                    }
                });
               ;
    }

    @Override
    public CompletableFuture<ResponseMessage> sendRpcRequest(ServiceEntity serviceEntity, RequestMessage request) {
        if (clientShutdownFuture.get() != null) {
            return FutureUtils.getFailedFuture(new IllegalStateException("client is already shut down."));
        }
        String address = serviceEntity.getAddress();
        EstablishedConnection connection = establishedConnections.get(address);
        if (connection != null) {
            return connection.sendRequest(request);
        } else {
            PendingConnection pendingConnection = pendingConnections.get(address);
            if (pendingConnection != null) {
                // There was a race, use the existing pending connection.
                return pendingConnection.sendRequest(request);
            } else {
                // We try to connect to the server.
                PendingConnection pending = new PendingConnection(address);
                PendingConnection previous = pendingConnections.putIfAbsent(address, pending);

                if (previous == null) {
                    // OK, we are responsible to connect.
                    String[] addressArr = StringUtils.split(address, ":");
                    Requires.requireTrue(addressArr.length == 2, "address is host:port");
                    bootstrap.connect(addressArr[0], Integer.parseInt(addressArr[1])).addListener(pending);
                    return pending.sendRequest(request);
                } else {
                    // There was a race, use the existing pending connection.
                    return previous.sendRequest(request);
                }
            }
        }
    }


    /**
     * Shuts down the client and closes all connections.
     *
     * <p>After a call to this method, all returned futures will be failed.
     *
     * @return A {@link CompletableFuture} that will be completed when the shutdown process is done.
     */
    public CompletableFuture<Void> shutdown() {
        final CompletableFuture<Void> newShutdownFuture = new CompletableFuture<>();
        if (clientShutdownFuture.compareAndSet(null, newShutdownFuture)) {

            final List<CompletableFuture<Void>> connectionFutures = new ArrayList<>();

            for (Map.Entry<String, EstablishedConnection> conn : establishedConnections.entrySet()) {
                if (establishedConnections.remove(conn.getKey(), conn.getValue())) {
                    connectionFutures.add(conn.getValue().close());
                }
            }

            for (Map.Entry<String, PendingConnection> conn : pendingConnections.entrySet()) {
                if (pendingConnections.remove(conn.getKey()) != null) {
                    connectionFutures.add(conn.getValue().close());
                }
            }

            CompletableFuture.allOf(
                    connectionFutures.toArray(new CompletableFuture<?>[connectionFutures.size()])
            ).whenComplete((result, throwable) -> {

                if (throwable != null) {
                    log.warn("Problem while shutting down the connections : ", throwable);
                }

                if (bootstrap != null) {
                    EventLoopGroup group = bootstrap.group();
                    if (group != null && !group.isShutdown()) {
                        group.shutdownGracefully(0L, 0L, TimeUnit.MILLISECONDS)
                                .addListener(finished -> {
                                    if (finished.isSuccess()) {
                                        newShutdownFuture.complete(null);
                                    } else {
                                        newShutdownFuture.completeExceptionally(finished.cause());
                                    }
                                });
                    } else {
                        newShutdownFuture.complete(null);
                    }
                } else {
                    newShutdownFuture.complete(null);
                }
            });

            return newShutdownFuture;
        }
        return clientShutdownFuture.get();
    }

    /**
     * A pending connection that is in the process of connecting.
     */
    private class PendingConnection implements ChannelFutureListener {

        /**
         * Lock to guard the connect call, channel hand in, etc.
         */
        private final Object connectLock = new Object();

        /**
         * Address of the server we are connecting to.
         */
        private final String serverAddress;

        /**
         * Queue of requests while connecting.
         */
        private final ArrayDeque<PendingRequest> queuedRequests = new ArrayDeque<>();

        /**
         * The established connection after the connect succeeds.
         */
        private EstablishedConnection established;

        /**
         * Atomic shut down future.
         */
        private final AtomicReference<CompletableFuture<Void>> connectionShutdownFuture = new AtomicReference<>(null);

        /**
         * Failure cause if something goes wrong.
         */
        private Throwable failureCause;

        /**
         * Creates a pending connection to the given server.
         *
         * @param serverAddress Address of the server to connect to.
         */
        private PendingConnection(
                final String serverAddress) {
            this.serverAddress = serverAddress;
        }

        @Override
        public void operationComplete(ChannelFuture future) throws Exception {
            if (future.isSuccess()) {
                handInChannel(future.channel());
            } else {
                close(future.cause());
            }
        }

        /**
         * Returns a future holding the serialized request result.
         *
         * <p>If the channel has been established, forward the call to the
         * established channel, otherwise queue it for when the channel is
         * handed in.
         *
         * @param request the request to be sent.
         * @return Future holding the serialized result
         */
        CompletableFuture<ResponseMessage> sendRequest(RequestMessage request) {
            synchronized (connectLock) {
                if (failureCause != null) {
                    return FutureUtils.getFailedFuture(failureCause);
                } else if (connectionShutdownFuture.get() != null) {
                    return FutureUtils.getFailedFuture(new ClosedChannelException());
                } else {
                    if (established != null) {
                        return established.sendRequest(request);
                    } else {
                        // Queue this and handle when connected
                        final PendingRequest pending = new PendingRequest(request);
                        queuedRequests.add(pending);
                        return pending;
                    }
                }
            }
        }

        

        /**
         * Hands in a channel after a successful connection.
         *
         * @param channel Channel to hand in
         */
        private void handInChannel(Channel channel) {
            synchronized (connectLock) {
                if (connectionShutdownFuture.get() != null || failureCause != null) {
                    // Close the channel and we are done. Any queued requests
                    // are removed on the close/failure call and after that no
                    // new ones can be enqueued.
                    channel.close();
                } else {
                    established = new EstablishedConnection(serverAddress, channel);

                    while (!queuedRequests.isEmpty()) {
                        final PendingRequest pending = queuedRequests.poll();

                        established.sendRequest(pending.request).whenComplete(
                                (response, throwable) -> {
                                    if (throwable != null) {
                                        pending.completeExceptionally(throwable);
                                    } else {
                                        pending.complete(response);
                                    }
                                });
                    }

                    // Publish the channel for the general public
                    establishedConnections.put(serverAddress, established);
                    pendingConnections.remove(serverAddress);

                    // Check shut down for possible race with shut down. We
                    // don't want any lingering connections after shut down,
                    // which can happen if we don't check this here.
                    if (clientShutdownFuture.get() != null) {
                        if (establishedConnections.remove(serverAddress, established)) {
                            established.close();
                        }
                    }
                }
            }
        }

        /**
         * Close the connecting channel with a ClosedChannelException.
         */
        private CompletableFuture<Void> close() {
            return close(new ClosedChannelException());
        }

        /**
         * Close the connecting channel with an Exception (can be {@code null})
         * or forward to the established channel.
         */
        private CompletableFuture<Void> close(Throwable cause) {
            CompletableFuture<Void> future = new CompletableFuture<>();
            if (connectionShutdownFuture.compareAndSet(null, future)) {
                synchronized (connectLock) {
                    if (failureCause == null) {
                        failureCause = cause;
                    }

                    if (established != null) {
                        established.close().whenComplete((result, throwable) -> {
                            if (throwable != null) {
                                future.completeExceptionally(throwable);
                            } else {
                                future.complete(null);
                            }
                        });
                    } else {
                        PendingRequest pending;
                        while ((pending = queuedRequests.poll()) != null) {
                            pending.completeExceptionally(cause);
                        }
                        future.complete(null);
                    }
                }
            }
            return connectionShutdownFuture.get();
        }

        @Override
        public String toString() {
            synchronized (connectLock) {
                return "PendingConnection{" +
                        "serverAddress=" + serverAddress +
                        ", queuedRequests=" + queuedRequests.size() +
                        ", established=" + (established != null) +
                        ", closed=" + (connectionShutdownFuture.get() != null) +
                        '}';
            }
        }

        /**
         * A pending request queued while the channel is connecting.
         */
        private final class PendingRequest extends CompletableFuture<ResponseMessage> {

            private final RequestMessage request;

            private PendingRequest(RequestMessage request) {
                this.request = request;
            }
        }
    }


    private class EstablishedConnection implements ClientHandlerCallback {

        /**
         * Address of the server we are connected to.
         */
        private final String serverAddress;

        /**
         * The actual TCP channel.
         */
        private final Channel channel;

        /**
         * Pending requests keyed by request ID.
         */
        private final ConcurrentHashMap<Long, TimestampedCompletableFuture> pendingRequests = new ConcurrentHashMap<>();

        /**
         * Current request number used to assign unique request IDs.
         */
        private final AtomicLong requestCount = new AtomicLong();

        /**
         * Atomic shut down future.
         */
        private final AtomicReference<CompletableFuture<Void>> connectionShutdownFuture = new AtomicReference<>(null);

        /**
         * Creates an established connection with the given channel.
         *
         * @param serverAddress Address of the server connected to
         * @param channel       The actual TCP channel
         */
        EstablishedConnection(
                final String serverAddress,
                final Channel channel) {

            this.serverAddress = Requires.requireNonNull(serverAddress);
            this.channel = Requires.requireNonNull(channel);

            // Add the client handler with the callback
            channel.pipeline().addLast(
                    new ClientHandler(this)
            );

        }

        /**
         * Close the channel with a ClosedChannelException.
         */
        CompletableFuture<Void> close() {
            return close(new ClosedChannelException());
        }

        /**
         * Close the channel with a cause.
         *
         * @param cause The cause to close the channel with.
         * @return Channel close future
         */
        private CompletableFuture<Void> close(final Throwable cause) {
            final CompletableFuture<Void> shutdownFuture = new CompletableFuture<>();

            if (connectionShutdownFuture.compareAndSet(null, shutdownFuture)) {
                channel.close().addListener(finished -> {
                    for (long requestId : pendingRequests.keySet()) {
                        TimestampedCompletableFuture pending = pendingRequests.remove(requestId);
                    }

                    // when finishing, if netty successfully closes the channel, then the provided exception is used
                    // as the reason for the closing. If there was something wrong at the netty side, then that exception
                    // is prioritized over the provided one.
                    if (finished.isSuccess()) {
                        shutdownFuture.completeExceptionally(cause);
                    } else {
                        log.warn("Something went wrong when trying to close connection due to : ", cause);
                        shutdownFuture.completeExceptionally(finished.cause());
                    }
                });
            }

            // in case we had a race condition, return the winner of the race.
            return connectionShutdownFuture.get();
        }

        /**
         * Returns a future holding the serialized request result.
         *
         * @param request the request to be sent.
         * @return Future holding the serialized result
         */
        CompletableFuture<ResponseMessage> sendRequest(RequestMessage request) {
            TimestampedCompletableFuture requestPromiseTs =
                    new TimestampedCompletableFuture(System.nanoTime());
            try {
                final long requestId = requestCount.getAndIncrement();
                request.setId(requestId);
                pendingRequests.put(requestId, requestPromiseTs);
                ByteBuf buf = MessageSerializer.getRequestBuf(request, rpcProperties.getClientProtocol());
                channel.writeAndFlush(buf).addListener((ChannelFutureListener) future -> {
                    if (!future.isSuccess()) {
                        // Fail promise if not failed to write
                        pendingRequests.remove(requestId);
                    }
                });

                // Check for possible race. We don't want any lingering
                // promises after a failure, which can happen if we don't check
                // this here. Note that close is treated as a failure as well.
                CompletableFuture<Void> clShutdownFuture = clientShutdownFuture.get();
                if (clShutdownFuture != null) {
                    TimestampedCompletableFuture pending = pendingRequests.remove(requestId);
                    if (pending != null) {
                        clShutdownFuture.whenComplete((ignored, throwable) -> {
                            if (throwable != null && pending.completeExceptionally(throwable)) {
                                //skip
                            } else {
                                // the shutdown future is always completed exceptionally so we should not arrive here.
                                // but in any case, we complete the pending connection request exceptionally.
                                pending.completeExceptionally(new ClosedChannelException());
                            }
                        });
                    }
                }
            } catch (Throwable t) {
                requestPromiseTs.completeExceptionally(t);
            }

            return requestPromiseTs;
        }

        @Override
        public void onRequestResult(long requestId, ResponseMessage response) {
            TimestampedCompletableFuture pending = pendingRequests.remove(requestId);
            if (pending != null && !pending.isDone()) {
                pending.complete(response);
            }
        }

        @Override
        public void onRequestFailure(long requestId, Throwable cause) {
            TimestampedCompletableFuture pending = pendingRequests.remove(requestId);
            if (pending != null && !pending.isDone()) {
                pending.completeExceptionally(cause);
            }
        }

        @Override
        public void onFailure(Throwable cause) {
            log.error("trace:",cause);
            close(cause).handle((cancelled, ignored) -> establishedConnections.remove(serverAddress, this));
        }

        @Override
        public String toString() {
            return "EstablishedConnection{" +
                    "serverAddress=" + serverAddress +
                    ", channel=" + channel +
                    ", pendingRequests=" + pendingRequests.size() +
                    ", requestCount=" + requestCount +
                    '}';
        }

        /**
         * Pair of promise and a timestamp.
         */
        private class TimestampedCompletableFuture extends CompletableFuture<ResponseMessage> {

            private final long timestampInNanos;

            TimestampedCompletableFuture(long timestampInNanos) {
                this.timestampInNanos = timestampInNanos;
            }

            public long getTimestamp() {
                return timestampInNanos;
            }
        }
    }

}
