package com.igroupes.rpc.client.request;

import com.igroupes.rpc.common.entity.ResponseMessage;
import com.igroupes.rpc.common.message.MessageSerializer;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.ReferenceCountUtil;
import lombok.extern.slf4j.Slf4j;

import java.nio.channels.ClosedChannelException;

@Slf4j
public class ClientHandler extends ChannelInboundHandlerAdapter {
    private ClientHandlerCallback callback;

    public ClientHandler(ClientHandlerCallback callback) {
        this.callback = callback;
    }


    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        try {
            ByteBuf buf = (ByteBuf) msg;
            ResponseMessage response = MessageSerializer.getResponse(buf);
            callback.onRequestResult(response.getId(),response);
        } catch (Throwable t1) {
            try {
                callback.onFailure(t1);
            } catch (Throwable t2) {
                log.error("Failed to notify callback about failure", t2);
            }
        } finally {
            ReferenceCountUtil.release(msg);
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        try {
            callback.onFailure(cause);
        } catch (Throwable t) {
            log.error("Failed to notify callback about failure", t);
        }
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        // Only the client is expected to close the channel. Otherwise it
        // indicates a failure. Note that this will be invoked in both cases
        // though. If the callback closed the channel, the callback must be
        // ignored.
        try {
            callback.onFailure(new ClosedChannelException());
        } catch (Throwable t) {
            log.error("Failed to notify callback about failure", t);
        }
    }
}
