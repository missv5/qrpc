package com.igroupes.rpc.client.request;

import com.igroupes.rpc.common.entity.RequestMessage;
import com.igroupes.rpc.common.entity.ResponseMessage;
import com.igroupes.rpc.common.entity.ServiceEntity;

import java.util.concurrent.CompletableFuture;

public interface RpcClient {

    CompletableFuture<ResponseMessage> sendRpcRequest(ServiceEntity serviceEntity , RequestMessage requestMessage);

}
