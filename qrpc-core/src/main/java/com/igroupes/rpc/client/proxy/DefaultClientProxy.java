package com.igroupes.rpc.client.proxy;

import com.igroupes.rpc.client.discovery.ServiceDiscovery;
import com.igroupes.rpc.client.request.RpcClient;
import com.igroupes.rpc.common.balance.LoadBalance;
import com.igroupes.rpc.common.entity.RequestMessage;
import com.igroupes.rpc.common.entity.ResponseMessage;
import com.igroupes.rpc.common.entity.ServiceEntity;
import com.igroupes.rpc.config.RpcProperties;
import com.igroupes.rpc.exception.QrpcException;
import com.igroupes.rpc.exception.ServiceNotFoundException;
import com.igroupes.rpc.utils.Requires;
import org.springframework.util.CollectionUtils;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

public class DefaultClientProxy implements ClientProxy {
    private final ServiceDiscovery discovery;
    private final LoadBalance loadBalance;
    private final RpcClient client;
    private final RpcProperties rpcProperties;

    public DefaultClientProxy(ServiceDiscovery discovery, LoadBalance loadBalance
            , RpcClient client, RpcProperties rpcProperties) {
        this.discovery = discovery;
        this.loadBalance = loadBalance;
        this.client = client;
        this.rpcProperties = rpcProperties;
    }


    @Override
    public Object getRpcProxy(String serviceName, Class<?> serviceClazz) {
        return Proxy.newProxyInstance(this.getClass().getClassLoader(), new Class[]{serviceClazz}, new ClientInvocationHandler(serviceName));
    }

    private class ClientInvocationHandler implements InvocationHandler {
        private final String serviceName;

        public ClientInvocationHandler(String serviceName) {
            this.serviceName = serviceName;
        }

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            CompletableFuture<ServiceEntity> serviceEntityCompletableFuture = CompletableFuture.supplyAsync(() -> getServiceEntity(serviceName));
            CompletableFuture<ResponseMessage> responseMessageCompletableFuture = serviceEntityCompletableFuture.thenComposeAsync(serviceEntity -> {
                RequestMessage requestMessage = createRequestMessage(serviceName, method, args);
                return client.sendRpcRequest(serviceEntity, requestMessage);
            });
            ResponseMessage responseMessage = responseMessageCompletableFuture.get(rpcProperties.getMaxRpcResponseMs() , TimeUnit.MILLISECONDS);
            if (responseMessage.getCode() != ResponseMessage.Code.OK.intValue()) {
                throw new QrpcException(responseMessage.getMessage());
            }
            return responseMessage.getResult();
        }
    }


    private RequestMessage createRequestMessage(String serviceName, Method method, Object[] args) {
        RequestMessage requestMessage = new RequestMessage();
        requestMessage.setServiceName(serviceName);
        requestMessage.setMethodName(method.getName());
        requestMessage.setParameters(args);
        requestMessage.setParameterClasses(method.getParameterTypes());
        return requestMessage;
    }

    private ServiceEntity getServiceEntity(String serviceName) {
        Requires.requireNonBlank(serviceName, "service name is blank");
        List<ServiceEntity> serviceList = discovery.getServiceList(serviceName);
        if (CollectionUtils.isEmpty(serviceList)) {
            throw new ServiceNotFoundException(String.format("service name : %s not found", serviceName));
        }
        ServiceEntity serviceEntity = loadBalance.findOne(serviceList);
        Requires.requireNonNull(serviceEntity, "service entity is null");
        return serviceEntity;
    }
}
