package com.igroupes.rpc.client.request;

import com.igroupes.rpc.common.entity.ResponseMessage;

public interface ClientHandlerCallback {

    /**
     * Called on a successful request.
     *
     * @param requestId ID of the request
     * @param response  The received response
     */
    void onRequestResult(long requestId, ResponseMessage response);

    /**
     * Called on a failed request.
     *
     * @param requestId ID of the request
     * @param cause     Cause of the request failure
     */
    void onRequestFailure(long requestId, Throwable cause);

    /**
     * Called on any failure, which is not related to a specific request.
     *
     * <p>This can be for example a caught Exception in the channel pipeline
     * or an unexpected channel close.
     *
     * @param cause Cause of the failure
     */
    void onFailure(Throwable cause);

}