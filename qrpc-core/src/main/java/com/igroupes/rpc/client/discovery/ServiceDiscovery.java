package com.igroupes.rpc.client.discovery;

import com.igroupes.rpc.common.entity.ServiceEntity;

import java.util.List;

/**
 * 服务发现
 */
public interface ServiceDiscovery {
    List<ServiceEntity> getServiceList(String serviceName);
}
