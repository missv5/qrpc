package com.igroupes.rpc.client.discovery;

import com.igroupes.rpc.common.entity.ServiceEntity;
import com.igroupes.rpc.config.RpcProperties;
import com.igroupes.rpc.exception.QrpcException;
import com.igroupes.rpc.serializer.Serializer;
import com.igroupes.rpc.serializer.SerializerFactory;
import com.igroupes.rpc.utils.RpcDiscoveryUtils;
import com.igroupes.rpc.utils.ZookeeperUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.curator.framework.CuratorFramework;
import org.apache.zookeeper.Watcher;

import java.util.Base64;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Slf4j
public class ZookeeperServiceDiscovery implements ServiceDiscovery {
    private RpcProperties rpcProperties;
    private Map<String, List<ServiceEntity>> serviceEntityMap = null;


    public ZookeeperServiceDiscovery(RpcProperties rpcProperties) {
        this.rpcProperties = rpcProperties;
        this.serviceEntityMap = new ConcurrentHashMap<>();
    }

    @Override
    public  List<ServiceEntity> getServiceList(String serviceName) {
        List<ServiceEntity> cacheEntity = serviceEntityMap.get(serviceName);
        if(cacheEntity != null){
            return cacheEntity;
        }
        CuratorFramework client = ZookeeperUtils.startCuratorFramework(rpcProperties);
        try {
            final Serializer<ServiceEntity> serializer = SerializerFactory.getInstance(rpcProperties.getServiceEntitySerializer(), ServiceEntity.class);
            List<String> serviceEntityList = client.getChildren().usingWatcher((Watcher) watchedEvent -> {
                try {
                    List<String> list = client.getChildren().forPath(RpcDiscoveryUtils.getServiceZKChildPath(serviceName));
                    if (list == null) {
                        serviceEntityMap.put(serviceName, Collections.EMPTY_LIST);
                    } else {
                        serviceEntityMap.put(serviceName, convertServiceEntityList(list, serializer));
                    }
                } catch (Exception ex) {
                    log.error("obtain service list fail:", ex);
                    throw new QrpcException("obtain service list fail");
                }
            }).forPath(RpcDiscoveryUtils.getServiceZKChildPath(serviceName));
            serviceEntityMap.putIfAbsent(serviceName,convertServiceEntityList(serviceEntityList, serializer));
            return serviceEntityMap.get(serviceName);
        } catch (Exception ex) {
            log.error("obtain service list fail:", ex);
            throw new QrpcException("obtain service list fail");
        }
    }


    private List<ServiceEntity> convertServiceEntityList(List<String> serviceDescribe, Serializer<ServiceEntity> serializer) {
        return serviceDescribe.stream().map(serviceEntityJSON ->
                serializer.deserialize(Base64.getDecoder().decode(serviceEntityJSON))
        ).collect(Collectors.toList());
    }
}
