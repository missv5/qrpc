package com.igroupes.rpc.utils;

import com.igroupes.rpc.common.constant.RpcConstant;

public enum RpcDiscoveryUtils {
    ;


    public static String getServiceZKChildPath(String serviceName) {
        Requires.requireNonBlank(serviceName, "service name is blank");
        return  RpcConstant.ZK_SERVICE_LIST_PATH + RpcConstant.PATH_SEPARATOR + serviceName;
    }


}
