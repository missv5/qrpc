package com.igroupes.rpc.utils;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public enum ReflectUtils {
    ;

    /**
     * 获取第num个泛型参数类型
     * @param clazz
     * @param num
     * @param <T>
     * @return
     */
    public static <T> Class<T> getTemplateType(Class<?> clazz, int num) {
        return (Class<T>) getSuperTemplateTypes(clazz)[num];
    }

    private static Class<?>[] getSuperTemplateTypes(Class<?> clazz) {
        Type type = clazz.getGenericSuperclass();
        while (true) {
            if (type instanceof ParameterizedType) {
                return getTemplateTypes((ParameterizedType) type);
            }

            if (clazz.getGenericSuperclass() == null) {
                throw new IllegalArgumentException();
            }

            type = clazz.getGenericSuperclass();
            clazz = clazz.getSuperclass();
        }
    }


    private static Class<?>[] getTemplateTypes(ParameterizedType parameterizedType) {
        Class<?>[] types = new Class<?>[parameterizedType.getActualTypeArguments().length];
        int i = 0;
        for (Type templateArgument : parameterizedType.getActualTypeArguments()) {
            assert templateArgument instanceof Class<?>;
            types[i++] = (Class<?>) templateArgument;
        }
        return types;
    }
}
