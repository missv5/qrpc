package com.igroupes.rpc.utils;

import com.igroupes.rpc.config.RpcProperties;
import com.igroupes.rpc.exception.ConfigurationException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;

@Slf4j
public class ZookeeperUtils {


    /**
     *
     * 创建zk客户端
     *
     */
    public static CuratorFramework startCuratorFramework(RpcProperties configuration) {
        Requires.requireNonNull(configuration, "configuration");
        String zkQuorum = configuration.getZookeeperQuorum();

        if (zkQuorum == null || StringUtils.isBlank(zkQuorum)) {
            throw new ConfigurationException("No valid ZooKeeper quorum has been specified. ");
        }

        int sessionTimeout = configuration.getZookeeperSessionTimeOut();

        int connectionTimeout = configuration.getZookeeperConnectTimeOut();

        int retryWait = configuration.getZookeeperRetryWait();

        int maxRetryAttempts = configuration.getZookeeperMaxRetryAttempts();


        String namespace = configuration.getZookeeperNamespace();

        log.info("Using '{}' as Zookeeper namespace.", namespace);

        CuratorFramework cf = CuratorFrameworkFactory.builder()
                .connectString(zkQuorum)
                .sessionTimeoutMs(sessionTimeout)
                .connectionTimeoutMs(connectionTimeout)
                .retryPolicy(new ExponentialBackoffRetry(retryWait, maxRetryAttempts))
                .namespace(namespace)
                .build();

        cf.start();

        return cf;
    }

}
