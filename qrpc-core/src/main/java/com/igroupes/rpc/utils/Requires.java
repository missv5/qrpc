package com.igroupes.rpc.utils;

import org.apache.commons.lang3.StringUtils;

public enum Requires {
    ;


    public static <T> T requireNonNull(T obj) {
        if (obj == null) {
            throw new NullPointerException();
        }
        return obj;
    }

    public static <T> T requireNonNull(T obj, String message) {
        if (obj == null) {
            throw new NullPointerException(message);
        }
        return obj;
    }


    public static String requireNonBlank(String obj) {
        if (StringUtils.isBlank(obj)) {
            throw new IllegalArgumentException();
        }
        return obj;
    }


    public static String requireNonBlank(String obj, String message) {
        if (StringUtils.isBlank(obj)) {
            throw new IllegalArgumentException(message);
        }
        return obj;
    }


    public static void requireTrue(boolean expression) {
        if (!expression) {
            throw new IllegalArgumentException();
        }
    }

    public static void requireTrue(boolean expression, Object message) {
        if (!expression) {
            throw new IllegalArgumentException(String.valueOf(message));
        }
    }


}
