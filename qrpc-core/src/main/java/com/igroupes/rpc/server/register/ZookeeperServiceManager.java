package com.igroupes.rpc.server.register;

import com.igroupes.rpc.common.constant.RpcConstant;
import com.igroupes.rpc.common.entity.ServiceEntity;
import com.igroupes.rpc.config.RpcProperties;
import com.igroupes.rpc.exception.QrpcException;
import com.igroupes.rpc.exception.ServiceNameExistsException;
import com.igroupes.rpc.serializer.Serializer;
import com.igroupes.rpc.serializer.SerializerFactory;
import com.igroupes.rpc.utils.RpcDiscoveryUtils;
import com.igroupes.rpc.utils.ZookeeperUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.curator.framework.CuratorFramework;
import org.apache.zookeeper.CreateMode;

import java.net.InetAddress;
import java.util.Base64;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
public class ZookeeperServiceManager implements ServiceManager {
    private RpcProperties rpcProperties;
    // serviceName->bean
    private Map<String, Object> beanMap = new ConcurrentHashMap<>();

    public ZookeeperServiceManager(RpcProperties rpcProperties) {
        this.rpcProperties = rpcProperties;
    }

    @Override
    public void register(String serviceName, Object service) {
        if (beanMap.putIfAbsent(serviceName, service) != null) {
            throw new ServiceNameExistsException(String.format("service name : %s already exists", serviceName));
        }
        CuratorFramework client = ZookeeperUtils.startCuratorFramework(rpcProperties);
        try {
            String servicePath = RpcDiscoveryUtils.getServiceZKChildPath(serviceName);
            if (client.checkExists().forPath(servicePath) == null) {
                client.create().creatingParentsIfNeeded().withMode(CreateMode.PERSISTENT).forPath(servicePath);
            }
            ServiceEntity serviceEntity = createServiceEntity(serviceName, service);
            Serializer<ServiceEntity> serializer = SerializerFactory.getInstance(rpcProperties.getServiceEntitySerializer(), ServiceEntity.class);
            String entityPath =  new String(Base64.getEncoder().encode(serializer.serialize(serviceEntity)),RpcConstant.DEFAULT_ENCODING);
            String path = servicePath + RpcConstant.PATH_SEPARATOR + entityPath;
            if (client.checkExists().forPath(path) != null) {
                client.delete().forPath(path);
            }
            client.create().withMode(CreateMode.EPHEMERAL).forPath(path);
        } catch (Exception ex) {
            log.error("service name : {} register fail", serviceName);
            log.error("trace :",ex);
            throw new QrpcException(String.format("service name : %s register fail", serviceName));
        }
    }

    private ServiceEntity createServiceEntity(String serviceName, Object service) {
        ServiceEntity serviceEntity = null;
        try {
            serviceEntity = new ServiceEntity();
            serviceEntity.setAddress(InetAddress.getLocalHost().getHostAddress()+":"+rpcProperties.getServerPort());
            serviceEntity.setName(serviceName);
            serviceEntity.setProtocol(rpcProperties.getServerProtocol());
            serviceEntity.setWeight(rpcProperties.getServerWight());
        } catch (Exception ex) {
            throw new QrpcException(ex.getMessage());
        }
        return serviceEntity;
    }

    @Override
    public Object getService(String serviceName) {
        return beanMap.get(serviceName);
    }
}
