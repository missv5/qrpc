package com.igroupes.rpc.server.register;

public interface ServiceManager {

    void register(String serviceName , Object service);

    Object getService(String serviceName);
}
