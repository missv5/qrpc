package com.igroupes.rpc.server.response;

import com.igroupes.rpc.config.RpcProperties;
import com.igroupes.rpc.exception.QrpcException;
import com.igroupes.rpc.server.register.ServiceManager;
import com.igroupes.rpc.utils.ExecutorUtils;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.stream.ChunkedWriteHandler;
import lombok.extern.slf4j.Slf4j;

import java.net.InetSocketAddress;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicReference;

@Slf4j
public class NettyRpcServer implements RpcServer {
    private RpcProperties rpcProperties;
    private ServerBootstrap bootstrap;
    private ExecutorService handleRequestExecutor;
    private ServiceManager serviceManager;
    private final AtomicReference<CompletableFuture<Void>> serverShutdownFuture = new AtomicReference<>(null);


    public NettyRpcServer(RpcProperties rpcProperties, ServiceManager serviceManager) {
        this.rpcProperties = rpcProperties;
        this.serviceManager = serviceManager;
    }

    @Override
    public void start() {
        final ThreadFactory threadFactory = new ThreadFactoryBuilder()
                .setDaemon(true)
                .setNameFormat("service EventLoop Thread %d")
                .build();
        int numEventLoopThreads = rpcProperties.getServerNumEventLoopThreads();
        final NioEventLoopGroup nioGroup = new NioEventLoopGroup(numEventLoopThreads, threadFactory);

        this.handleRequestExecutor = createRequestExecutor();
        int port = rpcProperties.getServerPort();
        this.bootstrap = new ServerBootstrap()
                .localAddress("localhost", port)
                .group(nioGroup)
                .channel(NioServerSocketChannel.class)
                .childHandler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel channel) throws Exception {
                        channel.pipeline()
                                .addLast(new LengthFieldBasedFrameDecoder(Integer.MAX_VALUE, 0, 4, 0, 4))
                                .addLast(new ChunkedWriteHandler())
                                .addLast(new ServerChannelHandler(handleRequestExecutor, serviceManager, rpcProperties.getServerProtocol()));
                    }
                });



        try {
            final ChannelFuture future = bootstrap.bind().sync();
            if (!future.isSuccess()) {
                log.error("server bind fail :", future.cause());
                throw new QrpcException("server bind fail  ");
            }
            final InetSocketAddress localAddress = (InetSocketAddress) future.channel().localAddress();
            log.info("server bind {}:{} success",localAddress.getHostName() , localAddress.getPort());
        } catch (Exception e) {
            log.debug("Failed to start server on port {}: {}.", port, e.getMessage());
            try {
                shutdownServer()
                        .whenComplete((ignoredV, ignoredT) -> serverShutdownFuture.getAndSet(null))
                        .get();
            } catch (Exception r) {
                log.warn("Problem while shutting down :", r);
            }
        }
    }


    private ExecutorService createRequestExecutor() {
        ThreadFactory threadFactory = new ThreadFactoryBuilder()
                .setDaemon(true)
                .setNameFormat("request handler Thread %d")
                .build();
        final int numHandleRequestThreads = rpcProperties.getServerHandleRequestThreads();
        return Executors.newFixedThreadPool(numHandleRequestThreads, threadFactory);
    }


    public CompletableFuture<Void> shutdownServer() {
        CompletableFuture<Void> shutdownFuture = new CompletableFuture<>();
        if (serverShutdownFuture.compareAndSet(null, shutdownFuture)) {
            log.info("Shutting down  server");

            final CompletableFuture<Void> groupShutdownFuture = new CompletableFuture<>();
            if (bootstrap != null) {
                EventLoopGroup group = bootstrap.group();
                if (group != null && !group.isShutdown()) {
                    group.shutdownGracefully(0L, 0L, TimeUnit.MILLISECONDS)
                            .addListener(finished -> {
                                if (finished.isSuccess()) {
                                    groupShutdownFuture.complete(null);
                                } else {
                                    groupShutdownFuture.completeExceptionally(finished.cause());
                                }
                            });
                } else {
                    groupShutdownFuture.complete(null);
                }
            } else {
                groupShutdownFuture.complete(null);
            }

            final CompletableFuture<Void> queryExecShutdownFuture = CompletableFuture.runAsync(() -> {
                if (handleRequestExecutor != null) {
                    ExecutorUtils.gracefulShutdown(10L, TimeUnit.MINUTES, handleRequestExecutor);
                }
            });

            CompletableFuture.allOf(
                    queryExecShutdownFuture, groupShutdownFuture
            ).whenComplete((result, throwable) -> {
                if (throwable != null) {
                    shutdownFuture.completeExceptionally(throwable);
                } else {
                    shutdownFuture.complete(null);
                }
            });
        }
        return serverShutdownFuture.get();
    }

}
