package com.igroupes.rpc.server.response;

import com.igroupes.rpc.common.entity.RequestMessage;
import com.igroupes.rpc.common.entity.ResponseMessage;
import com.igroupes.rpc.common.message.MessageSerializer;
import com.igroupes.rpc.exception.RpcServerException;
import com.igroupes.rpc.server.register.ServiceManager;
import com.igroupes.rpc.utils.ExceptionUtils;
import com.igroupes.rpc.utils.Requires;
import io.netty.buffer.ByteBuf;
import io.netty.channel.*;
import io.netty.util.ReferenceCountUtil;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

@Slf4j
@ChannelHandler.Sharable
public class ServerChannelHandler extends ChannelInboundHandlerAdapter {
    private ExecutorService handleRequestExecutor;
    private ServiceManager serviceManager;
    private String protocol;

    public ServerChannelHandler(ExecutorService handleRequestExecutor, ServiceManager serviceManager, String protocol) {
        this.handleRequestExecutor = handleRequestExecutor;
        this.serviceManager = serviceManager;
        this.protocol = protocol;
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
            RequestMessage request = null;
            ResponseMessage responseMessage = null;
            try {
                ByteBuf buf = (ByteBuf) msg;
                request = MessageSerializer.getRequest(buf);
                handleRequestExecutor.submit(new AsyncRequestTask(this, ctx, request.getId(), request));
            } catch (Throwable th) {
                // 创建错误的response对象
                log.error("handle request message :{} fail", request);
                log.error("trace:", th);
                responseMessage = ResponseMessage.error(request.getId(), ResponseMessage.Code.INVOKE_EXCEPTION, th.getMessage());
                ctx.writeAndFlush(MessageSerializer.getResponseBuf(responseMessage, protocol));
            } finally {
                ReferenceCountUtil.release(msg);
            }
        }



    private CompletableFuture<ResponseMessage> handleRequest(RequestMessage request) {
        CompletableFuture<ResponseMessage> completableFuture = new CompletableFuture<>();
        String serviceName = request.getServiceName();
        Object service = serviceManager.getService(serviceName);
        if (service == null) {
            completableFuture.completeExceptionally(new RpcServerException(String.format("service name : %s not found", serviceName), ResponseMessage.Code.SERVICE_NAME_NOT_FOUND));
        }
        Method method = null;
        try {
            method = service.getClass().getMethod(request.getMethodName(), request.getParameterClasses());
        } catch (NoSuchMethodException ex) {
            String classesDescribe = "";
            if (request.getParameterClasses().length > 0) {
                StringBuilder builder = new StringBuilder("");
                Arrays.stream(request.getParameterClasses()).forEach(parameterClass -> {
                    builder.append(parameterClass.getCanonicalName()).append(",");
                });
                classesDescribe = builder.substring(0, builder.length() - 1);
            }
            completableFuture.completeExceptionally(new RpcServerException(String.format("method name : %s with parameter types : [%s] not found",
                    request.getMethodName(), classesDescribe), ResponseMessage.Code.METHOD_NOT_FOUND));
        }
        try {
            Object result = method.invoke(service, request.getParameters());
            completableFuture.complete(ResponseMessage.ok(request.getId(), result));
        } catch (Exception ex) {
            completableFuture.completeExceptionally(new RpcServerException(ex.getMessage(), ResponseMessage.Code.INVOKE_EXCEPTION));
        }
        return completableFuture;
    }


    /**
     * Task to execute the actual query against the state instance.
     */
    private  class AsyncRequestTask implements Runnable {

        private final ServerChannelHandler handler;

        private final ChannelHandlerContext ctx;

        private final long requestId;

        private final RequestMessage request;

        private final long creationNanos;


        AsyncRequestTask(
                final ServerChannelHandler handler,
                final ChannelHandlerContext ctx,
                final long requestId,
                final RequestMessage request) {

            this.handler = Requires.requireNonNull(handler);
            this.ctx = Requires.requireNonNull(ctx);
            this.requestId = requestId;
            this.request = Requires.requireNonNull(request);
            this.creationNanos = System.nanoTime();
        }

        @Override
        public void run() {

            if (!ctx.channel().isActive()) {
                return;
            }

            handler.handleRequest(request).whenComplete((resp, throwable) -> {
                ResponseMessage res = null;
                try {
                    if (throwable != null) {
                        res = throwable instanceof RpcServerException
                                ? new ResponseMessage(requestId, ((RpcServerException) throwable).getCode().intValue(), throwable.getMessage())
                                : new ResponseMessage(requestId, ResponseMessage.Code.INVOKE_EXCEPTION.intValue(), throwable.getMessage());
                    }else{
                        if (resp == null) {
                            res = new ResponseMessage(requestId, ResponseMessage.Code.INVOKE_EXCEPTION.intValue(), "NULL returned for request with ID " + requestId + ".");
                        } else {
                            res = resp;
                        }
                    }
                } catch (Throwable t) {
                        final String errMsg = "Failed request " + requestId + "." + System.lineSeparator() + " Caused by: " + ExceptionUtils.stringifyException(t);
                        res = new ResponseMessage(requestId, ResponseMessage.Code.INVOKE_EXCEPTION.intValue(), errMsg);
                }
                final ByteBuf serialResp = MessageSerializer.getResponseBuf(res,protocol);
                ctx.writeAndFlush(serialResp).addListener(new RequestWriteListener());

            });
        }

        @Override
        public String toString() {
            return "AsyncRequestTask{" +
                    "requestId=" + requestId +
                    ", request=" + request +
                    '}';
        }

        /**
         * Callback after query result has been written.
         *
         * <p>Gathers stats and logs errors.
         */
        private class RequestWriteListener implements ChannelFutureListener {

            @Override
            public void operationComplete(ChannelFuture future) throws Exception {
                long durationNanos = System.nanoTime() - creationNanos;
                long durationMillis = TimeUnit.MILLISECONDS.convert(durationNanos, TimeUnit.NANOSECONDS);

                if (future.isSuccess()) {
                    log.debug("Request {} was successfully answered after {} ms.", request, durationMillis);
                } else {
                    log.debug("Request {} failed after {} ms due to: {}", request, durationMillis, future.cause());
                }
            }
        }
    }


    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        final String msg = "Exception in server pipeline. Caused by: " + ExceptionUtils.stringifyException(cause);
        ResponseMessage responseMessage = ResponseMessage.error(-1, ResponseMessage.Code.INVOKE_EXCEPTION, cause.getMessage());
        log.debug(msg);
        ctx.writeAndFlush(MessageSerializer.getResponseBuf(responseMessage, protocol)).addListener(ChannelFutureListener.CLOSE);
    }

}
