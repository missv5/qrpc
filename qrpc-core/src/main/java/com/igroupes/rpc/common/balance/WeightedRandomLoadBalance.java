package com.igroupes.rpc.common.balance;

import com.igroupes.rpc.common.entity.ServiceEntity;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class WeightedRandomLoadBalance extends OptimizeLoadBalance {

    @Override
    protected ServiceEntity findBestOne(List<ServiceEntity> serviceEntityList) {
        List<ServiceEntity> newServiceEntityList = weightedServiceEntityList(serviceEntityList);
        ThreadLocalRandom current = ThreadLocalRandom.current();
        return newServiceEntityList.get(current.nextInt(newServiceEntityList.size()));
    }
}
