package com.igroupes.rpc.common.entity;

import lombok.Data;

@Data
public class ResponseMessage extends MessageBody {
    private Object result;
    private String message;
    private int code;

    public ResponseMessage(long id , int code, String message) {
        this.id = id;
        this.code = code;
        this.message = message;
    }

    public ResponseMessage(long id , Object result, String message, int code) {
        this.id = id;
        this.result = result;
        this.message = message;
        this.code = code;
    }


    public static ResponseMessage error(long id , Code code, String message) {
        return new ResponseMessage(id,code.intValue, message);
    }

    public static ResponseMessage ok(long id , Object result) {
        return new ResponseMessage(id,result, null, Code.OK.intValue);
    }

    public enum Code {
        OK(0),
        SERVICE_NAME_NOT_FOUND(100),
        METHOD_NOT_FOUND(101),
        INVOKE_EXCEPTION(102),
        ;
        int intValue = 0;

        Code(int intValue) {
            this.intValue = intValue;
        }

        public int intValue() {
            return intValue;
        }
    }
}
