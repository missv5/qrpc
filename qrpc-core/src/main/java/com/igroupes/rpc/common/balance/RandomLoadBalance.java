package com.igroupes.rpc.common.balance;

import com.igroupes.rpc.common.entity.ServiceEntity;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class RandomLoadBalance extends OptimizeLoadBalance {

    @Override
    public ServiceEntity findBestOne(List<ServiceEntity> serviceEntityList) {
        ThreadLocalRandom current = ThreadLocalRandom.current();
        return serviceEntityList.get(current.nextInt(serviceEntityList.size()));
    }
}
