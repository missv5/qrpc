package com.igroupes.rpc.common.balance;

import com.igroupes.rpc.common.entity.ServiceEntity;
import com.igroupes.rpc.exception.ServiceNotFoundException;
import com.igroupes.rpc.utils.Requires;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

public abstract class OptimizeLoadBalance implements LoadBalance {
    @Override
    public final ServiceEntity findOne(List<ServiceEntity> serviceEntityList) {
        if (CollectionUtils.isEmpty(serviceEntityList)) {
            throw new ServiceNotFoundException("service not found");
        }
        if (serviceEntityList.size() == 1) {
            return serviceEntityList.get(0);
        }
        return findBestOne(serviceEntityList);
    }

    protected abstract ServiceEntity findBestOne(List<ServiceEntity> serviceEntityList);

    protected List<ServiceEntity> weightedServiceEntityList(List<ServiceEntity> serviceEntityList){
        List<ServiceEntity> newServiceEntityList = new ArrayList<>();
        for (ServiceEntity serviceEntity : serviceEntityList) {
            Requires.requireTrue(serviceEntity.getWeight() > 0, "service weight must more than 0");
            for (int i = 0; i < serviceEntity.getWeight(); i++) {
                newServiceEntityList.add(serviceEntity);
            }
        }
        return newServiceEntityList;
    }

}
