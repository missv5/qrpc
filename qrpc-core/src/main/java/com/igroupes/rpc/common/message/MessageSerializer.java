package com.igroupes.rpc.common.message;

import com.igroupes.rpc.common.entity.RequestMessage;
import com.igroupes.rpc.common.entity.ResponseMessage;
import com.igroupes.rpc.serializer.Serializer;
import com.igroupes.rpc.serializer.SerializerFactory;
import com.igroupes.rpc.serializer.SerializerType;
import com.igroupes.rpc.utils.Requires;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

public enum MessageSerializer {
    ;

    public static ResponseMessage getResponse(ByteBuf buf) {
        return getMessage(buf, ResponseMessage.class);
    }

    public static RequestMessage getRequest(ByteBuf buf) {
        return getMessage(buf, RequestMessage.class);
    }


    public static ByteBuf getRequestBuf(RequestMessage request,String protocol) {
        return getMessageBuf(request,protocol);
    }

    public static ByteBuf getResponseBuf(ResponseMessage response,String protocol) {
        return getMessageBuf(response,protocol);
    }


    private static <T> T getMessage(ByteBuf buf, Class<T> clazz) {
        SerializerType serializerType = SerializerType.values()[buf.readByte()];
        Serializer<T> serializer = SerializerFactory.getInstance(serializerType.name(), clazz);
        byte[] data = new byte[buf.readableBytes()];
        buf.readBytes(data);
        return serializer.deserialize(data);
    }

    private static <T> ByteBuf getMessageBuf(T message,String protocol) {
        Serializer<T> serializer = SerializerFactory.getInstance(protocol,(Class<T>) message.getClass());
        byte[] data = serializer.serialize(message);
        final int SERIAL_TYPE_LEN = 1;
        int frameLength= SERIAL_TYPE_LEN + data.length;
        ByteBuf buf = Unpooled.buffer(frameLength+Integer.BYTES);
        int ordinal = SerializerType.getByName(protocol).ordinal();
        Requires.requireTrue(ordinal <= Byte.MAX_VALUE, "serializer type is byte range");
        buf.writeInt(frameLength);
        buf.writeByte(ordinal);
        buf.writeBytes(data);
        return buf;
    }
}
