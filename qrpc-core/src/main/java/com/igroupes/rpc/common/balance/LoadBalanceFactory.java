package com.igroupes.rpc.common.balance;

import com.igroupes.rpc.utils.Requires;

public enum LoadBalanceFactory {
    ;

    public static LoadBalance getInstance(String type) {
        LoadBalanceType loadBalanceType = LoadBalanceType.getByName(type);
        Requires.requireNonNull(loadBalanceType, "load balance type is null");
        switch (loadBalanceType) {
            case RANDOM:
                return new RandomLoadBalance();
            case ROUND_ROBIN:
                return new RoundRobinLoadBalance();
            case WEIGHT_RANDOM:
                return new WeightedRandomLoadBalance();
            case WEIGHT_ROUND_ROBIN:
                return new WeightedRoundRobinLoadBalance();
            default:
                throw new UnsupportedOperationException();
        }
    }
}
