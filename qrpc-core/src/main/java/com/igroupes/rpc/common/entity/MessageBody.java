package com.igroupes.rpc.common.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class MessageBody  implements Serializable {
    protected long id;
}
