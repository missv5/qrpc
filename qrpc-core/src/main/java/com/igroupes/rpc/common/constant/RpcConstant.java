package com.igroupes.rpc.common.constant;

public interface RpcConstant {
    String ZK_RPC_SERVICE_ROOT_PATH = "/qrpc";
    String ZK_SERVICE_LIST_PATH = "/service_list";
    String PATH_SEPARATOR = "/";

    String DEFAULT_ENCODING = "utf-8";
}
