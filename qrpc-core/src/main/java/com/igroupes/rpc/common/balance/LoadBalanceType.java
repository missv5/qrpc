package com.igroupes.rpc.common.balance;

public enum LoadBalanceType {
    RANDOM("random"), ROUND_ROBIN("round_robin")
    ,WEIGHT_RANDOM("weight_random"),WEIGHT_ROUND_ROBIN("weight_round_robin");
    private String name;

    private LoadBalanceType(String name) {
        this.name = name;
    }

    public static LoadBalanceType getByName(String name) {
        return LoadBalanceType.valueOf(name.toUpperCase());
    }
}
