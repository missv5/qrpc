package com.igroupes.rpc.common.entity;

import lombok.Data;


@Data
public class RequestMessage extends MessageBody {
    private String serviceName;
    private String methodName;
    private Class<?>[] parameterClasses;
    private Object[] parameters;
    private RequestHeader header;

    @Data
    public static class RequestHeader{

    }
}
