package com.igroupes.rpc.common;

import com.igroupes.rpc.client.proxy.ClientProxy;
import com.igroupes.rpc.config.annotation.Reference;
import com.igroupes.rpc.config.annotation.Service;
import com.igroupes.rpc.exception.InvalidServiceException;
import com.igroupes.rpc.exception.QrpcException;
import com.igroupes.rpc.server.register.ServiceManager;
import com.igroupes.rpc.server.response.RpcServer;
import com.igroupes.rpc.utils.Requires;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.Map;

@Slf4j
public class RpcStartListener implements ApplicationListener<ContextRefreshedEvent> {
    private final ClientProxy clientProxy;
    private final ServiceManager serviceRegister;
    private final RpcServer rpcServer;

    public RpcStartListener(ClientProxy clientProxy, ServiceManager serviceRegister, RpcServer rpcServer) {
        this.clientProxy = clientProxy;
        this.serviceRegister = serviceRegister;
        this.rpcServer = rpcServer;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        ApplicationContext applicationContext = contextRefreshedEvent.getApplicationContext();
        startClientRpc(applicationContext);
        startServerRpc(applicationContext);
    }


    private void startServerRpc(ApplicationContext applicationContext) {
        Map<String, Object> services = applicationContext.getBeansWithAnnotation(Service.class);
        if (!CollectionUtils.isEmpty(services)) {
            // 服务启动
            rpcServer.start();
            // 注册服务
            services.values().stream().forEach(service -> {
                checkService(service);
                String serviceName = getServiceName(service, applicationContext);
                serviceRegister.register(serviceName, service);
            });
        }
    }

    private String getServiceName(Object service, ApplicationContext applicationContext) {
        Service serviceAnno = applicationContext.getBean(service.getClass()).getClass().getAnnotation(Service.class);
        if (!StringUtils.isEmpty(serviceAnno.value())) {
            return serviceAnno.value();
        }
        Class<?>[] interfaces = service.getClass().getInterfaces();
        if (interfaces.length > 1) {
            throw new InvalidServiceException("service only implements a interface or set value by @Service");
        }
        return interfaces[0].getCanonicalName();

    }

    private void checkService(Object service) {
        Requires.requireNonNull(service, "service is null");
        int classModifier = service.getClass().getModifiers();
        if (Modifier.isAbstract(classModifier) || Modifier.isInterface(classModifier)) {
            throw new InvalidServiceException("service must a implements class");
        }
        if (service.getClass().getInterfaces().length == 0) {
            throw new InvalidServiceException("service must  implements a interface");
        }

    }

    private void startClientRpc(ApplicationContext applicationContext) {
        String[] beanDefinitionNames = applicationContext.getBeanDefinitionNames();
        if (beanDefinitionNames.length == 0) {
            return;
        }
        final String[] exludes = {"org.springframework"};
        Arrays.stream(beanDefinitionNames).filter(beanName -> {
                    boolean test = Arrays.stream(exludes).filter(exludePackage -> !beanName.startsWith(exludePackage)).count() == exludes.length;
                    return test;
                }
        ).forEach(beanName -> {
            Object bean = applicationContext.getBean(beanName);
            Class<?> clazz = bean.getClass();
            Field[] declaredFields = clazz.getDeclaredFields();
            Arrays.stream(declaredFields).forEach(field -> {
                Reference reference = field.getAnnotation(Reference.class);
                if (reference != null) {
                    try {
                        field.setAccessible(true);
                        Class<?> serviceClazz = field.getType();
                        String serviceName = StringUtils.isEmpty(reference.value()) ? serviceClazz.getCanonicalName() : reference.value();
                        field.set(bean, clientProxy.getRpcProxy(serviceName, serviceClazz));
                    } catch (IllegalAccessException ex) {
                        log.error("trace:",ex);
                        throw new QrpcException(ex.getMessage());
                    }
                }
            });
        });
    }


}
