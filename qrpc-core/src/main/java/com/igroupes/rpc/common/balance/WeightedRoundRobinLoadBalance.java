package com.igroupes.rpc.common.balance;

import com.igroupes.rpc.common.entity.ServiceEntity;

import java.util.List;

public class WeightedRoundRobinLoadBalance extends OptimizeLoadBalance{
    private int pos = 0;

    @Override
    protected synchronized ServiceEntity findBestOne(List<ServiceEntity> serviceEntityList) {
        List<ServiceEntity> newServiceEntityList = weightedServiceEntityList(serviceEntityList);
        if (pos >= newServiceEntityList.size()) {
            pos = 0;
        }
        return serviceEntityList.get(pos++);
    }
}
