package com.igroupes.rpc.common.balance;

import com.igroupes.rpc.common.entity.ServiceEntity;

import java.util.List;

/**
 * 轮询
 */
public class RoundRobinLoadBalance extends OptimizeLoadBalance {
    private int pos = 0;

    @Override
    public synchronized ServiceEntity findBestOne(List<ServiceEntity> serviceEntityList) {
        if (pos >= serviceEntityList.size()) {
            pos = 0;
        }
        return serviceEntityList.get(pos++);
    }
}
