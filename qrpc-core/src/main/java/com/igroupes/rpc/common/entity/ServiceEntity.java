package com.igroupes.rpc.common.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class ServiceEntity implements Serializable {
    private String name;
    private String protocol;
    private String address;
    private int weight;
}
