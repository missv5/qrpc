package com.igroupes.rpc.common.balance;

import com.igroupes.rpc.common.entity.ServiceEntity;

import java.util.List;

public interface LoadBalance {
    ServiceEntity findOne(List<ServiceEntity> serviceEntityList);
}
