package com.igroupes.rpc.serializer;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.esotericsoftware.kryo.pool.KryoPool;
import com.igroupes.rpc.exception.SerializeException;
import org.objenesis.strategy.StdInstantiatorStrategy;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

public class KryoSerializer<T> implements Serializer<T> {

    private static final KryoPool kryopool = new KryoPool.Builder(() -> {
        final Kryo kryo = new Kryo();
        kryo.setInstantiatorStrategy(new Kryo.DefaultInstantiatorStrategy(
                new StdInstantiatorStrategy()));
        return kryo;
    }).softReferences().build();


    @Override
    public byte[] serialize(T record) throws SerializeException {
        Kryo kryo = null;
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            Output output = new Output(byteArrayOutputStream);
            kryo = kryopool.borrow();
            kryo.writeClassAndObject(output, record);
            byte[] bytes = output.toBytes();
            output.flush();
            return bytes;
        } catch (Exception ex) {
            throw new SerializeException(ex.getMessage());
        } finally {
            if (kryo != null) {
                kryopool.release(kryo);
            }
        }

    }

    @Override
    public T deserialize(byte[] serialData) throws SerializeException {
        Kryo kryo = null;
        try {
            ByteArrayInputStream bin = new ByteArrayInputStream(serialData);
            Input input = new Input(bin);
            kryo = kryopool.borrow();
            T response = (T) kryo.readClassAndObject(input);
            input.close();
            return response;
        } catch (Exception ex) {
            throw new SerializeException(ex.getMessage());
        } finally {
            if (kryo != null) {
                kryopool.release(kryo);
            }
        }
    }
}