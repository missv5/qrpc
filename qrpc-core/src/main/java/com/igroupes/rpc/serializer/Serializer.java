package com.igroupes.rpc.serializer;

import com.igroupes.rpc.exception.SerializeException;


public interface Serializer<R> {

    byte[] serialize(R record) throws SerializeException;

    R deserialize(byte[] serialData) throws SerializeException;
}
