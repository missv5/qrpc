package com.igroupes.rpc.serializer;

/**
 * rpc服务协议
 */
public enum SerializerType {
    JDK("jdk"), JSON("json"), KRYO("kryo");
    private String name;

    private SerializerType(String name) {
        this.name = name;
    }

    public static SerializerType getByName(String name) {
        return SerializerType.valueOf(name.toUpperCase());
    }
}
