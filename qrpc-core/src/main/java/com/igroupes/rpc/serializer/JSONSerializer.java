package com.igroupes.rpc.serializer;

import com.alibaba.fastjson.JSON;
import com.igroupes.rpc.common.constant.RpcConstant;
import com.igroupes.rpc.exception.SerializeException;
import com.igroupes.rpc.utils.ReflectUtils;

import java.io.UnsupportedEncodingException;

public class JSONSerializer<T> implements Serializer<T> {


    @Override
    public byte[] serialize(T record) throws SerializeException {
        try {
            return JSON.toJSONString(record).getBytes(RpcConstant.DEFAULT_ENCODING);
        } catch (UnsupportedEncodingException ex) {
            throw new SerializeException(ex.getMessage());
        }
    }

    @Override
    public T deserialize(byte[] target) throws SerializeException {
        try {
            return JSON.parseObject(new String(target, RpcConstant.DEFAULT_ENCODING), ReflectUtils.getTemplateType(this.getClass(), 0));
        } catch (UnsupportedEncodingException ex) {
            throw new SerializeException(ex.getMessage());
        }
    }

}
