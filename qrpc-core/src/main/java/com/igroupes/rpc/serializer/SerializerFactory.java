package com.igroupes.rpc.serializer;

public enum SerializerFactory {
    ;


    public static <T> Serializer<T> getInstance(String name,Class<T> clazz) {
        SerializerType serializerType = SerializerType.getByName(name);
        switch (serializerType) {
            case JDK:
                return new JDKSerializer<T>();
            case JSON:
                return new JSONSerializer<T>();
            case KRYO:
                return new KryoSerializer<T>();
            default:
                throw new UnsupportedOperationException();
        }
    }
}
