package com.igroupes.rpc.serializer;

import com.igroupes.rpc.exception.SerializeException;
import com.igroupes.rpc.utils.Requires;

import java.io.*;

public class JDKSerializer<T> implements Serializer<T> {

    @Override
    public byte[] serialize(T record) throws SerializeException {
        Requires.requireTrue(record instanceof Serializable, "record must implement Serializable");
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream();
             ObjectOutputStream oos = new ObjectOutputStream(baos)) {
            oos.writeObject(record);
            oos.flush();
            return baos.toByteArray();
        } catch (Exception ex) {
            throw new SerializeException(ex.getMessage());
        }
    }

    @Override
    public T deserialize(byte[] target) throws SerializeException {
        try (ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(target))) {
            return (T) in.readObject();
        } catch (Exception ex) {
            throw new SerializeException(ex.getMessage());
        }
    }
}
