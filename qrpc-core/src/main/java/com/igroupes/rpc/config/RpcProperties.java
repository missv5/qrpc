package com.igroupes.rpc.config;


import com.igroupes.rpc.common.balance.LoadBalanceType;
import com.igroupes.rpc.serializer.SerializerType;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "qrpc")
@Data
public class RpcProperties {
    private String serviceEntitySerializer = SerializerType.JDK.name();
    private int serverPort = 10591;
    private String clientProtocol = SerializerType.JDK.name();
    private String serverProtocol = SerializerType.JDK.name();
    private int serverWight = 1;
    private String zookeeperQuorum;
    private int zookeeperSessionTimeOut = 10000;
    private int zookeeperConnectTimeOut = 10000;
    private int zookeeperRetryWait = 10000;
    private int zookeeperMaxRetryAttempts = 3;
    private String zookeeperNamespace = "qrpc";
    private String loadBalanceType = LoadBalanceType.RANDOM.name();
    private int clientNumEventLoopThreads = 3;
    private int serverNumEventLoopThreads = 3;
    private int serverHandleRequestThreads = 3;
    private int maxRpcResponseMs = 60000;
}
