package com.igroupes.rpc.config;

import com.igroupes.rpc.client.discovery.ServiceDiscovery;
import com.igroupes.rpc.client.discovery.ZookeeperServiceDiscovery;
import com.igroupes.rpc.client.proxy.ClientProxy;
import com.igroupes.rpc.client.proxy.DefaultClientProxy;
import com.igroupes.rpc.client.request.NettyRpcClient;
import com.igroupes.rpc.client.request.RpcClient;
import com.igroupes.rpc.common.RpcStartListener;
import com.igroupes.rpc.common.balance.LoadBalance;
import com.igroupes.rpc.common.balance.LoadBalanceFactory;
import com.igroupes.rpc.server.register.ServiceManager;
import com.igroupes.rpc.server.register.ZookeeperServiceManager;
import com.igroupes.rpc.server.response.NettyRpcServer;
import com.igroupes.rpc.server.response.RpcServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(RpcProperties.class)
public class QrpcAutoConfiguration {

    @Bean
    public RpcProperties rpcProperties() {
        return new RpcProperties();
    }

    @Bean
    public ServiceManager serviceManager(@Autowired RpcProperties rpcProperties) {
        return new ZookeeperServiceManager(rpcProperties);
    }

    @Bean
    public LoadBalance loadBalance(@Autowired RpcProperties rpcProperties) {
        return LoadBalanceFactory.getInstance(rpcProperties.getLoadBalanceType());
    }

    @Bean
    public ServiceDiscovery serviceDiscovery(@Autowired RpcProperties rpcProperties) {
        return new ZookeeperServiceDiscovery(rpcProperties);
    }

    @Bean
    public RpcClient rpcClient(@Autowired RpcProperties rpcProperties) {
        return new NettyRpcClient(rpcProperties);
    }


    @Bean
    public ClientProxy clientProxy(@Autowired ServiceDiscovery serviceDiscovery, @Autowired LoadBalance loadBalance
            , @Autowired RpcClient rpcClient,@Autowired RpcProperties rpcProperties) {
        return new DefaultClientProxy(serviceDiscovery, loadBalance, rpcClient,rpcProperties);
    }

    @Bean
    public RpcServer rpcServer(@Autowired RpcProperties rpcProperties, @Autowired ServiceManager serviceManager) {
        return new NettyRpcServer(rpcProperties, serviceManager);
    }

    @Bean
    public RpcStartListener rpcStartListener(@Autowired ClientProxy clientProxy, @Autowired ServiceManager serviceManager,
                                             @Autowired RpcServer rpcServer) {
        return new RpcStartListener(clientProxy, serviceManager, rpcServer);
    }
}
